﻿/*
 * 4.	Write a program which recursively creates 10 threads.
 * Each thread should be with the same body and receive a state with integer number, decrement it,
 * print and pass as a state into the newly created thread.
 * Use Thread class for this task and Join for waiting threads.
 * 
 * Implement all of the following options:
 * - a) Use Thread class for this task and Join for waiting threads.
 * - b) ThreadPool class for this task and Semaphore for waiting threads.
 */

using System;
using System.Threading;

namespace MultiThreading.Task4.Threads.Join
{
    class Program
    {
        private static Semaphore semaphore = new Semaphore(0, 1);
        static void Main(string[] args)
        {
            Console.WriteLine("4.	Write a program which recursively creates 10 threads.");
            Console.WriteLine("Each thread should be with the same body and receive a state with integer number, decrement it, print and pass as a state into the newly created thread.");
            Console.WriteLine("Implement all of the following options:");
            Console.WriteLine();
            Console.WriteLine("- a) Use Thread class for this task and Join for waiting threads.");
            Console.WriteLine("- b) ThreadPool class for this task and Semaphore for waiting threads.");

            Console.WriteLine();

            Rec(10);

            Console.ReadKey();
            Console.WriteLine();

            RecB(10);

            Console.ReadLine();
        }

        private static void CallBody(object obj)
        {
	        Console.WriteLine(obj);
        }

        private static void Rec(int num)
        {
            if (num == 0)
               return;

            Thread thread = new Thread(CallBody);
            thread.Start(num);
            thread.Join();

            Rec(Interlocked.Decrement(ref num));
        }

        private static void RecB(int num)
        {
            if (num == 0)
                return;

            ThreadPool.QueueUserWorkItem(CallBodyB, num);
            semaphore.WaitOne();

            RecB(Interlocked.Decrement(ref num));
        }

        private static void CallBodyB(object obj)
        {
            Console.WriteLine(obj);
            semaphore.Release();
        }
    }
}
