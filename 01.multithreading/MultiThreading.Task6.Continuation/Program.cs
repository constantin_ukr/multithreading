﻿/*
*  Create a Task and attach continuations to it according to the following criteria:
   a.    Continuation task should be executed regardless of the result of the parent task.
   b.    Continuation task should be executed when the parent task finished without success.
   c.    Continuation task should be executed when the parent task would be finished with fail and parent task thread should be reused for continuation
   d.    Continuation task should be executed outside of the thread pool when the parent task would be cancelled
   Demonstrate the work of the each case with console utility.
*/
using System;
using System.Threading;
using System.Threading.Tasks;

namespace MultiThreading.Task6.Continuation
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Create a Task and attach continuations to it according to the following criteria:");
            Console.WriteLine("a.    Continuation task should be executed regardless of the result of the parent task.");
            Console.WriteLine("b.    Continuation task should be executed when the parent task finished without success.");
            Console.WriteLine("c.    Continuation task should be executed when the parent task would be finished with fail and parent task thread should be reused for continuation.");
            Console.WriteLine("d.    Continuation task should be executed outside of the thread pool when the parent task would be cancelled.");
            Console.WriteLine("Demonstrate the work of the each case with console utility.");
            Console.WriteLine();

            // feel free to add your code

            Console.WriteLine("A.");
            var taskA = new Task(async () =>
            {
                Console.WriteLine("A start");
                await Task.Delay(1000);
                Console.WriteLine("A finish");
            });
            taskA.ContinueWith( _ => Console.WriteLine("AC"));

            taskA.Start();

            Console.ReadKey();

            Console.WriteLine("B.");
            var taskB = new Task(() => {
                Console.WriteLine("B");
                Thread.Sleep(10);
                throw null;
            });
            taskB.ContinueWith( _ => Console.WriteLine("BC"), TaskContinuationOptions.OnlyOnFaulted);

            taskB.Start();

            Console.ReadKey();

            Console.WriteLine("C.");
            var taskC = new Task(() => { 
               Console.WriteLine("C");
               Thread.Sleep(10);
               throw null;
            });
            taskC.ContinueWith(t =>
            {
                if (t.IsFaulted)
                { 
                    Console.WriteLine("CC");
                }
            }, TaskContinuationOptions.AttachedToParent);

            taskC.Start();

            Console.ReadKey();

            Console.WriteLine("D.");

            var tokenSource = new CancellationTokenSource(500);
            CancellationToken ct = tokenSource.Token;

            Task taskD = new Task(() =>
            {
                Console.WriteLine("D start");
                while (true)
                {
                    Thread.Sleep(10);
                    if (ct.IsCancellationRequested)
                    {
                        ct.ThrowIfCancellationRequested();
                        break;
                    }
                }

                Console.WriteLine("D finish");
                }, ct);

            taskD.ContinueWith(_ => Console.WriteLine("DC"), TaskContinuationOptions.OnlyOnCanceled);

            taskD.Start();

            Console.ReadLine();
        }
    }
}
