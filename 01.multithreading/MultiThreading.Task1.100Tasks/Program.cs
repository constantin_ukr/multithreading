﻿/*
 * 1.	Write a program, which creates an array of 100 Tasks, runs them and waits all of them are not finished.
 * Each Task should iterate from 1 to 1000 and print into the console the following string:
 * “Task #0 – {iteration number}”.
 */
using System;
using System.Threading.Tasks;

namespace MultiThreading.Task1._100Tasks
{
    class Program
    {
        const int TaskAmount = 10;
        const int MaxIterationsCount = 100;

        static void Main(string[] args)
        {
            Console.WriteLine(".Net Mentoring Program. Multi threading V1.");
            Console.WriteLine("1.	Write a program, which creates an array of 100 Tasks, runs them and waits all of them are not finished.");
            Console.WriteLine("Each Task should iterate from 1 to 1000 and print into the console the following string:");
            Console.WriteLine("“Task #0 – {iteration number}”.");
            Console.WriteLine();
            
            HundredTasks();

            Console.ReadLine();
        }

        static void HundredTasks()
        {
            // feel free to add your code here
            Task[] tanks = new Task[TaskAmount];
            Action<object> tankPrinter = itr =>
            {
                for (int j = 0; j < MaxIterationsCount; j++)
                {
                    Output((int)itr, j);
                }
            };

			for (int i = 0; i < TaskAmount; i++)
			{
                tanks[i] = Task.Factory.StartNew(tankPrinter, i);
            }
            
            Task.WaitAll(tanks);

            /*Parallel.For(0, TaskAmount, i =>
            {
                for (int j = 0; j < MaxIterationsCount; j++)
                {
                    Output(i, j);
                }
            });*/
        }

        static void Output(int taskNumber, int iterationNumber)
        {
            Console.WriteLine($"Task #{taskNumber} – {iterationNumber}");
        }
    }
}
