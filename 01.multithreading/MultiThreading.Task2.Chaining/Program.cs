﻿/*
 * 2.	Write a program, which creates a chain of four Tasks.
 * First Task – creates an array of 10 random integer.
 * Second Task – multiplies this array with another random integer.
 * Third Task – sorts this array by ascending.
 * Fourth Task – calculates the average value. All this tasks should print the values to console.
 */
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace MultiThreading.Task2.Chaining
{
    class Program
    {
        private const int ArrSize = 10;
        private static Random _rnd = new Random();

        static void Main(string[] args)
        {
            Console.WriteLine(".Net Mentoring Program. MultiThreading V1 ");
            Console.WriteLine("2.	Write a program, which creates a chain of four Tasks.");
            Console.WriteLine("First Task – creates an array of 10 random integer.");
            Console.WriteLine("Second Task – multiplies this array with another random integer.");
            Console.WriteLine("Third Task – sorts this array by ascending.");
            Console.WriteLine("Fourth Task – calculates the average value. All this tasks should print the values to console");
            Console.WriteLine();

            // feel free to add your code

            Task<int[]> task1 = new Task<int[]>(
                () =>
                {
                    var arr = CreateRandomArray();
                    Print(arr);
                    return arr;
                });

            var task2 = task1.ContinueWith(next =>
            {
                var arr = next.Result;
                arr = CreateRandomArray();
                Print(arr);
                return arr;
            }, TaskContinuationOptions.OnlyOnRanToCompletion);

            var task3 = task2.ContinueWith(next =>
            {
                var arr = next.Result;
                Array.Sort(arr);
                Print(arr);
                return arr;
            }, TaskContinuationOptions.OnlyOnRanToCompletion);

            task3.ContinueWith(next =>
            {
                Console.WriteLine(Environment.NewLine + next.Result.Average());
            }, TaskContinuationOptions.OnlyOnRanToCompletion);

            task1.Start();

            Console.ReadLine();
        }

        private static void Print(int[] arr)
        {
            Console.WriteLine();
            for (int i = 0; i < arr.Length; i++)
            {
                Console.Write( $"{arr[i]} ");
            }
        }

        private static int[] CreateRandomArray()
        {
            var arr = new int[ArrSize];
            for (int i = 0; i < ArrSize; i++)
            {
                arr[i] = _rnd.Next(0, 100);
                Thread.Sleep(1);
            }
            return arr;
        }
    }
}
