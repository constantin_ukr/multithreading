﻿/*
 * 5. Write a program which creates two threads and a shared collection:
 * the first one should add 10 elements into the collection and the second should print all elements
 * in the collection after each adding.
 * Use Thread, ThreadPool or Task classes for thread creation and any kind of synchronization constructions.
 */
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace MultiThreading.Task5.Threads.SharedCollection
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("5. Write a program which creates two threads and a shared collection:");
            Console.WriteLine("the first one should add 10 elements into the collection and the second should print all elements in the collection after each adding.");
            Console.WriteLine("Use Thread, ThreadPool or Task classes for thread creation and any kind of synchronization constructions.");
            Console.WriteLine();

            // feel free to add your code
            const int MaxIter = 10;
            List<int> list = new List<int>();

            Semaphore semaphore = new Semaphore(0, 1);

            Task w = new Task(() =>
            {
                for (int i = 0; i < MaxIter; i++)
                {
                    list.Add(i);
                    semaphore.WaitOne();
                }
            });

            Task r = new Task(() =>
            {
                int itr = 0;
                do
                {
                    Console.WriteLine();
                    for (int i = 0; i < list.Count; i++)
                    {
                        Console.Write($" {i}");
                    }
                    ++itr;
                    semaphore.Release();
                } while (itr < MaxIter);
            });

            w.Start();
            r.Start();

            Console.ReadLine();
        }
    }
}
