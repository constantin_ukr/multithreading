﻿/*
 * Create a class based on ExpressionVisitor, which makes expression tree transformation:
 * 1. converts expressions like <variable> + 1 to increment operations, <variable> - 1 - into decrement operations.
 * 2. changes parameter values in a lambda expression to constants, taking the following as transformation parameters:
 *    - source expression;
 *    - dictionary: <parameter name: value for replacement>
 * The results could be printed in console or checked via Debugger using any Visualizer.
 */
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace ExpressionTrees.Task1.ExpressionsTransformer
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Expression Visitor for increment/decrement.");
            Console.WriteLine();

            Expression<Func<int, int>> exp = (x) => x + (x + 1) * (x - 2) - (x - 1) + (1+1-1);

            Console.WriteLine(exp);
            Console.WriteLine(exp.Compile().Invoke(5));

            var res = new IncDecExpressionVisitor().VisitAndConvert(exp, "");
            Console.WriteLine(res);
            Console.WriteLine(res.Compile().Invoke(5));

            Console.WriteLine();

            Expression<Func<int, int, int, int>> trans = (x, y, z) => x * x + (y - x + 4) * z;
            Console.WriteLine(trans);
            Console.WriteLine(trans.Compile().Invoke(1, 10, 5));

            var transformer = new TransformerParamExpressionVisitor(new Dictionary<string, int> { { "x", 1 }, { "y", 10 } });
            var resTrans = transformer.VisitAndConvert(trans, "");
            Console.WriteLine(resTrans);
            Console.WriteLine(resTrans.Compile().Invoke(0, 0, 5));

            Console.ReadLine();
        }
    }
}
