﻿using System;
using System.Linq.Expressions;

namespace ExpressionTrees.Task1.ExpressionsTransformer
{
	public class IncDecExpressionVisitor : ExpressionVisitor
	{
		protected override Expression VisitBinary(BinaryExpression node)
		{
			ParameterExpression p = null;

			if (node.NodeType == ExpressionType.Add)
			{
				if (IsValidNode(node, out p))
				{
					return Expression.Increment(p);
				}
			}
			else if(node.NodeType == ExpressionType.Subtract)
			{
				if (IsValidNode(node, out p))
				{
					return Expression.Decrement(p);
				}
			}

			return base.VisitBinary(node);
		}

		private bool IsValidNode(BinaryExpression node, out ParameterExpression p)
		{
			return (IsParamExp(node.Left, out p) && IsConstExp(node.Right)) ||
				(IsParamExp(node.Right, out p) && IsConstExp(node.Left));
		}

		private bool IsParamExp(Expression exp, out ParameterExpression p)
		{
			p = null;
			if (exp.NodeType == ExpressionType.Parameter && exp.Type == typeof(int))
			{
				p = (ParameterExpression) exp;
				return true;
			}

			return false;
		}

		private bool IsConstExp(Expression exp)
		{
			return exp.NodeType == ExpressionType.Constant && exp.Type == typeof(int) && Convert.ToInt32(((ConstantExpression)exp).Value) == 1;
		}
	}
}
