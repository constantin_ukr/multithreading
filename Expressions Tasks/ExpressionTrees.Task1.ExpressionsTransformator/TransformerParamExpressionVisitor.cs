﻿using System.Collections.Generic;
using System.Linq.Expressions;

namespace ExpressionTrees.Task1.ExpressionsTransformer
{
	internal class TransformerParamExpressionVisitor : ExpressionVisitor
	{
		private readonly IReadOnlyDictionary<string, int> _source;

		public TransformerParamExpressionVisitor(Dictionary<string, int> source)
		{
			_source = source;
		}

		protected override Expression VisitLambda<T>(Expression<T> node)
		{
			return Expression.Lambda(Visit(node.Body), node.Parameters);
		}

		protected override Expression VisitParameter(ParameterExpression node)
		{
			if (_source.TryGetValue(node.Name, out var val))
			{
				return Expression.Constant(val, val.GetType());
			}

			return base.VisitParameter(node);
		}
	}
}
