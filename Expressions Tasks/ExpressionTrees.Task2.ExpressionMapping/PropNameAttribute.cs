﻿using System;

namespace ExpressionTrees.Task2.ExpressionMapping
{
	[AttributeUsage(AttributeTargets.Property)]
	public class PropNameAttribute : Attribute
	{
		public string Name { get; }

		public PropNameAttribute(string name)
		{
			Name = name;
		}
	}
}
