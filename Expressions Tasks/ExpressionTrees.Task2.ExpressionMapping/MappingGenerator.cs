﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace ExpressionTrees.Task2.ExpressionMapping
{
	public class MappingGenerator
	{
		public Mapper<TSource, TDestination> Generate<TSource, TDestination>()
		{
			var sourceParam = Expression.Parameter(typeof(TSource));
			var bindings = new List<MemberBinding>();
			var source = typeof(TSource);
			var dest = typeof(TDestination);

			foreach (var prop in source.GetProperties().Where( p => p.CanRead))
			{
				var target = dest.GetProperty(prop.Name);
				if (target == null)
				{
					var propName = GetMetaTypeName(prop);
					target = dest.GetProperty(propName);
				}
				if (target != null && target.CanWrite && target.PropertyType.IsAssignableFrom(prop.PropertyType))
				{
					bindings.Add(Expression.Bind(target, Expression.Property(sourceParam, prop)));
				}
			}

			return new Mapper<TSource, TDestination>(
				Expression.Lambda<Func<TSource, TDestination>>(
					Expression.MemberInit(
						Expression.New(typeof(TDestination)),
						bindings),
					sourceParam)
				.Compile());
		}
		private static string GetMetaTypeName(PropertyInfo prop)
		{
			var attributes = prop.GetCustomAttributes(typeof(PropNameAttribute), false);

			if (attributes.Length == 0)
				return string.Empty;

			return ((PropNameAttribute)attributes[0]).Name;
		}
	}
}
