using ExpressionTrees.Task2.ExpressionMapping.Tests.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ExpressionTrees.Task2.ExpressionMapping.Tests
{
    [TestClass]
    public class ExpressionMappingTests
    {
        // todo: add as many test methods as you wish, but they should be enough to cover basic scenarios of the mapping generator

        [TestMethod]
        public void TestMethod1()
        {
	        var id = 12;
	        var name = "user";
	        byte age = 21;

            var mapGenerator = new MappingGenerator();
            var mapper = mapGenerator.Generate<Foo, Bar>();

            var res = mapper.Map(new Foo{Id = id, Name = name, Age = age});

            Assert.AreEqual(res.Id, id);
            Assert.AreEqual(res.Name, name);
            Assert.AreEqual(res.Age, age);
            Assert.IsNull(res.Surname);
        }

        [TestMethod]
        public void Task2Test()
        {
	        var id = 12;
	        var name = "user";
	        var test1 = 9;
	        var test2 = 777;

	        var mapGenerator = new MappingGenerator();
	        var mapper = mapGenerator.Generate<Source, Target>();

	        var res = mapper.Map(new Source{ Id = id, SourceName = name, Test1 = test1, Test2 = test2});

	        Assert.AreEqual(res.Key, id);
	        Assert.AreEqual(res.Name, name);
            Assert.AreEqual(res.Test1, test1);
            Assert.AreEqual(res.Test3, default(int));
        }
    }
}
