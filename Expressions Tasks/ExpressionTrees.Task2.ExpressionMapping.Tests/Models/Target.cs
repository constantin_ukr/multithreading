﻿namespace ExpressionTrees.Task2.ExpressionMapping.Tests.Models
{
	public class Target
	{
		public int Key { get; set; }
		public string Name { get; set; }
		public int Test1 { get; set; }
		public int Test3 { get; set; }
	}
}
