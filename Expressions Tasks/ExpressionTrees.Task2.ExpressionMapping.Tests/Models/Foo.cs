﻿namespace ExpressionTrees.Task2.ExpressionMapping.Tests.Models
{
    internal class Foo
    {
        // add here some properties
        public int Id { get; set; } = 10;
        public string Name { get; set; } = "User1";

        public byte Age { get; set; } = 33;
    }
}
