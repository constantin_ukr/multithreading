﻿namespace ExpressionTrees.Task2.ExpressionMapping.Tests.Models
{
    internal class Bar
    {
        // add here some other properties
        public int Id { get; set; }

        public string Name { get; set; }

        public string Surname { get; set; }

        public byte Age { get; set; }
    }
}
