﻿namespace ExpressionTrees.Task2.ExpressionMapping.Tests.Models
{
	public class Source
	{
		[PropName("Key")]
		public int Id { get; set; }

		[PropName("Name")]
		public string SourceName { get; set; }

		public int Test1 { get; set; }
		public int Test2 { get; set; }
	}
}
