﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Expressions.Task3.E3SQueryProvider
{
    public class ExpressionToFtsRequestTranslator : ExpressionVisitor
    {
        readonly StringBuilder _resultStringBuilder;

        public ExpressionToFtsRequestTranslator()
        {
            _resultStringBuilder = new StringBuilder();
        }

        public string Translate(Expression exp)
        {
            Visit(exp);

            return _resultStringBuilder.ToString();
        }

        #region protected methods

        protected override Expression VisitMethodCall(MethodCallExpression node)
        {
            if (node.Method.DeclaringType == typeof(Queryable)
                && node.Method.Name == "Where")
            {
                var predicate = node.Arguments[1];
                Visit(predicate);

                return node;
            }

			if (node.Method.DeclaringType == typeof(string))
			{
				switch (node.Method.Name)
				{
					case "StartsWith":
						Visit(node.Object);
						_resultStringBuilder.Append("(");
						Visit(node.Arguments[0]);
						_resultStringBuilder.Append("*)");
						return node;

					case "EndsWith":
						Visit(node.Object);
						_resultStringBuilder.Append("(*");
						Visit(node.Arguments[0]);
						_resultStringBuilder.Append(")");
						return node;

					case "Contains":
						Visit(node.Object);
						_resultStringBuilder.Append("(*");
						Visit(node.Arguments[0]);
						_resultStringBuilder.Append("*)");
						return node;

					case "Equals":
						Visit(node.Object);
						_resultStringBuilder.Append("(");
						Visit(node.Arguments[0]);
						_resultStringBuilder.Append(")");
						return node;
				}
			}

			return base.VisitMethodCall(node);
		}

        protected override Expression VisitBinary(BinaryExpression node)
        {
            switch (node.NodeType)
            {
                case ExpressionType.Equal:
					if (!IsValidForEquals(node, out var isFirstMemberAccess))
						return node;

					DoCorrectVisit(isFirstMemberAccess);
					break;

				case ExpressionType.AndAlso:
				case ExpressionType.And:
					_resultStringBuilder.Append("{ \"query\":\"");
					Visit(node.Left);
					_resultStringBuilder.Append("\"},");
					_resultStringBuilder.Append("{ \"query\":\"");
					Visit(node.Right);
					_resultStringBuilder.Append("\"}");
                    break;


                default:
                    throw new NotSupportedException($"Operation '{node.NodeType}' is not supported");
            };

            return node;

            void DoCorrectVisit(bool isFirstMemberAccess)
            {
	            if (isFirstMemberAccess)
	            {
		            Visit(node.Left);
		            _resultStringBuilder.Append("(");
		            Visit(node.Right);
		            _resultStringBuilder.Append(")");
                }
	            else
	            {
		            Visit(node.Right);
		            _resultStringBuilder.Append("(");
		            Visit(node.Left);
		            _resultStringBuilder.Append(")");
                }
            }
        }

        protected override Expression VisitMember(MemberExpression node)
        {
            _resultStringBuilder.Append(node.Member.Name).Append(":");

            return base.VisitMember(node);
        }

        protected override Expression VisitConstant(ConstantExpression node)
        {
            _resultStringBuilder.Append(node.Value);

            return node;
        }

        #endregion


        private bool IsValidForEquals(BinaryExpression node, out bool isFirstMemberAccess)
        {
	        ExpType e = node.Left.NodeType == ExpressionType.MemberAccess
		        ? ExpType.MemberAccess
		        : node.Left.NodeType == ExpressionType.Constant
			        ? ExpType.Constant : ExpType.None;

	        isFirstMemberAccess = e.HasFlag(ExpType.MemberAccess);

            e |= node.Right.NodeType == ExpressionType.MemberAccess
	            ? ExpType.MemberAccess
	            : node.Right.NodeType == ExpressionType.Constant
		            ? ExpType.Constant : ExpType.None;

            var v = e.HasFlag(ExpType.Both);
            return v;
        }

        [Flags]
        private enum ExpType
        {
            None,
            MemberAccess,
            Constant,
            Both = MemberAccess | Constant
        }
    }
}
