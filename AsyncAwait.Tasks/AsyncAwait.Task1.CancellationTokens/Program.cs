﻿/*
 * Изучите код данного приложения для расчета суммы целых чисел от 0 до N, а затем
 * измените код приложения таким образом, чтобы выполнялись следующие требования:
 * 1. Расчет должен производиться асинхронно.
 * 2. N задается пользователем из консоли. Пользователь вправе внести новую границу в процессе вычислений,
 * что должно привести к перезапуску расчета.
 * 3. При перезапуске расчета приложение должно продолжить работу без каких-либо сбоев.
 */

using System;
using System.Threading;
using System.Threading.Tasks;

namespace AsyncAwait.Task1.CancellationTokens
{
    class Program
    {
        /// <summary>
        /// The Main method should not be changed at all.
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            Console.WriteLine("Mentoring program L2. Async/await.V1. Task 1");
            Console.WriteLine("Calculating the sum of integers from 0 to N.");
            Console.WriteLine("Use 'q' key to exit...");
            Console.WriteLine();

            Console.WriteLine("Enter N: ");
            CancellationTokenSource tokenSource = new CancellationTokenSource();
            CancellationToken token = tokenSource.Token;
            Task calc = null;
            
            string input = Console.ReadLine();
            while (input.Trim().ToUpper() != "Q")
            {
                if (int.TryParse(input, out int n))
                {
                    if (calc!= null && calc.Status != TaskStatus.RanToCompletion)
                    { 
                        tokenSource.Cancel();
                        tokenSource = new CancellationTokenSource();
                        token = tokenSource.Token;
                        Console.WriteLine($"The task for {n} started... ");
                    }
                    calc = CalculateSum(n, token);
                }
                else
                {
                    Console.WriteLine($"Invalid integer: '{input}'. Please try again.");
                    Console.WriteLine("Enter N: ");
                }

                input = Console.ReadLine();
                
            }

            Console.WriteLine("Press any key to continue");
            Console.ReadLine();
        }

        private static async Task CalculateSum(int n, CancellationToken token)
        {
            // todo: make calculation asynchronous
            Task<long> sumTask = Calculator.Calculate(n, token);
            Console.WriteLine();
            Console.WriteLine("Enter N: ");

            var sum = await sumTask;
            if (token.IsCancellationRequested)
            {
                Console.WriteLine($"Sum for {n} cancelled... Now sum is {sum}");
                Console.WriteLine("Enter N: ");
            }
            else
            {
                Console.WriteLine($"Sum for {n} = {sum}.");
                Console.WriteLine("Enter N: ");
            }
        }
    }
}