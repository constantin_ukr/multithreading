﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace Profiling
{
	class Program
	{
		static void Main(string[] args)
		{
			Pswd pswd = new Pswd();
			var r = pswd.GeneratePasswordHashUsingSalt("test", Encoding.UTF8.GetBytes("testtesttesttesttest"));
			Console.WriteLine(r);
			Console.ReadKey();
		}

		class Pswd
		{
			public string GeneratePasswordHashUsingSalt(string passwordText, byte[] salt)
			{
				var iterate = 10000;
				var pbkdf2 = new Rfc2898DeriveBytes(passwordText, salt, iterate);
				//byte[] hash = pbkdf2.GetBytes(20);
				//byte[] hashBytes = new byte[36];
				//Array.Copy(salt, 0, hashBytes, 0, 16);
				//Array.Copy(hash, 0, hashBytes, 16, 20);
				var passwordHash = Convert.ToBase64String(pbkdf2.GetBytes(36));
				return passwordHash;
			}

		}
	}
}
