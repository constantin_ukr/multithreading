﻿using RabbitMQ.Client.Events;
using RabbitMQ.Client;
using System;
using System.Collections.Concurrent;
using System.IO;
using Ionic.Zip;
using System.Text;
using System.Collections.Generic;

namespace Server
{
	class Program
	{
		const string repositoryPath = "C:\\rabbitmqFileStore";

		private static ConcurrentDictionary<string, int> repo =
			new ConcurrentDictionary<string, int>();

		static void Main(string[] args)
		{
			if (!Directory.Exists(repositoryPath))
				Directory.CreateDirectory(repositoryPath);

			Console.WriteLine("Server started!");
			
			var factory = new ConnectionFactory() { HostName = "localhost" };
			using (var connection = factory.CreateConnection())
			using (var channel = connection.CreateModel())
			{
				channel.ExchangeDeclare(exchange: "ks_topic", type: ExchangeType.Topic);

				var queueName = channel.QueueDeclare().QueueName;

				channel.QueueBind(queue: queueName, exchange: "ks_topic", routingKey: "*.#");

				var consumer = new EventingBasicConsumer(channel);

				consumer.Received += OnConsumerReceived;

				channel.BasicConsume(queue: queueName, autoAck: true, consumer: consumer);

				Console.ReadLine();
				Console.WriteLine("Queue closed!");
				consumer.Received -= OnConsumerReceived;
			}

			Console.WriteLine("Server stopped!");
			Console.ReadKey();
		}

		private static void OnConsumerReceived(object sender, BasicDeliverEventArgs e)
		{
			Console.WriteLine("OnConsumerReceived");
			var body = e.Body.ToArray();
			var fileNameParts = e.RoutingKey.Split(".");
			var isBigFile = e.BasicProperties.MessageId != "0";
			var fullPath = !isBigFile
				? $"{repositoryPath}\\{fileNameParts[2]}.{fileNameParts[1]}"
				: $"{repositoryPath}\\{e.BasicProperties.CorrelationId}\\{fileNameParts[2]}.{fileNameParts[1]}";

			var errorMsg = "OK";
			var shouldResponse = false;
			try
			{
				ProcessFile(body, fullPath, e.BasicProperties.CorrelationId, e.BasicProperties.MessageId, isBigFile, out shouldResponse);
			}
			catch(Exception ex)
			{
				errorMsg = ex.Message;
			}

			if (shouldResponse)
			{
				ResponseToClient(errorMsg, e.BasicProperties.ReplyTo, e.BasicProperties.CorrelationId, isBigFile);
			}
		}

		private static void ProcessFile(byte[] body, string fullPath, string id, string messageId, bool isBigFile, out bool shouldResponse)
		{
			shouldResponse = true;
			Console.WriteLine($"Processing file {fullPath}");

			var path = Path.GetDirectoryName(fullPath);
			if (!string.IsNullOrWhiteSpace(path) && !Directory.Exists(path))
			{
				Directory.CreateDirectory(path);
			}

			using (var writer = new BinaryWriter(File.Open(fullPath, FileMode.Create)))
			{
				writer.Write(body);
			}

			Console.WriteLine($"Saved file: {fullPath}");
			if (!isBigFile)
				return;

			if (repo.ContainsKey(id))
			{
				var gotPart = repo[id];
				int max = Convert.ToInt32(messageId.Split("-")[1]);
				if (max - 1 == gotPart)
				{
					var p = $"{Path.GetDirectoryName(fullPath)}\\{id}.zip";
					using (ZipFile zip = ZipFile.Read(p))
					{
						Console.WriteLine($"Unzipping file {fullPath}");
						var newPath = Path.GetDirectoryName(fullPath.Replace($"{id}\\", ""));
						zip.ExtractAll(newPath);
					}

					Console.WriteLine($"delete directory: {repositoryPath}\\{id}");
					Directory.Delete($"{repositoryPath}\\{id}", true);
				}
				else
				{
					shouldResponse = false;
					repo[id] = gotPart + 1;
				}
			}
			else
			{
				shouldResponse = false;
				repo.TryAdd(id, 1);
			}
		}

		private static void ResponseToClient(string message, string routingKey, string corId, bool isBigFile)
		{
			var factory = new ConnectionFactory() { HostName = "localhost" };
			using (var connection = factory.CreateConnection())
			using (var channel = connection.CreateModel())
			{
				channel.ExchangeDeclare(exchange: "reply_queue", type: ExchangeType.Direct);

				var properteis = channel.CreateBasicProperties();
				properteis.Headers = new Dictionary<string, object> { { "big", isBigFile } };
				properteis.CorrelationId = corId;

				var body = Encoding.UTF8.GetBytes(message);

				channel.BasicPublish(exchange: "reply_queue",
									routingKey: routingKey,
									basicProperties: properteis,
									body: body);

				Console.WriteLine($"Send Response: curID: {corId} key: {routingKey}");
			}
		}
	}
}
