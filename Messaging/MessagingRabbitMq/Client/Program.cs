﻿using Ionic.Zip;
using System;
using System.Linq;
using System.IO;
using System.Threading.Tasks;
using Common;
using RabbitMQ.Client.Events;
using RabbitMQ.Client;
using System.Text;

namespace Client
{
	class Program
	{
		static void Main(string[] args)
		{
			Console.Write("Set folder path:");
			var path = Console.ReadLine();
			if (string.IsNullOrWhiteSpace(path) || !Directory.Exists(path))
			{
				path = @"C:\rabbitmq";
				Console.WriteLine($"was set default path '{path}'");
				if (!Directory.Exists(path))
				{
					Directory.CreateDirectory(path);
				}
			}
			else
			{
				Console.WriteLine($"using path '{path}'");
			}

			var files = Directory.GetFiles(path);
			if (files.Length > 0)
			{
				foreach (var file in files)
				{
					var uuid = UUIDGenerator.Generate(file);
					Console.WriteLine(file);
					if (!FileProcessingRepository.Exist(uuid))
					{
						Task.Run(() => ProcessFile(file));
					}
				}
			}

			using var watcher = CreateWatcher(path);

			Console.ReadLine();
		}

		private static FileSystemWatcher CreateWatcher(string path)
		{
			var watcher = new FileSystemWatcher(path);
			watcher.Created += OnCreated;
			watcher.IncludeSubdirectories = true;
			watcher.EnableRaisingEvents = true;

			return watcher;
		}

		private static async void OnCreated(object sender, FileSystemEventArgs e)
		{
			var uuid = UUIDGenerator.Generate(e.FullPath);
			if (FileProcessingRepository.Exist(uuid))
			{
				Console.WriteLine($"File {e.FullPath} is processing!");
				return;
			}
			
			Console.WriteLine($"Added new file: {e.Name}");
			await Task.Run(() => ProcessFile(e.FullPath));
		}

		private static void ProcessFile(string fullPath)
		{
			var fileInfo = new FileInfo(fullPath);

			if (fileInfo.Length < 1048576)
			{
				var body = File.ReadAllBytes(fullPath);
				var id = UUIDGenerator.Generate(fullPath);
				Console.WriteLine($"Start processing a file with id: {id}");
				var rKey = GetRoutingKey(fullPath);
				Publisher.Publish(id, rKey, body);

				FileProcessingRepository.AddRow(id, fullPath);
				CreateWaitingStatus(rKey, fullPath, id);
			}
			else
			{
				ProcessBigFile(fullPath);
			}
		}

		private static string GetRoutingKey(string fullPath)
		{
			var fileInfo = new FileInfo(fullPath);
			return $"{Environment.MachineName}.{fileInfo.Extension.Replace(".", "")}.{fileInfo.Name.Replace(fileInfo.Extension, "")}";
		}

		private static void ProcessBigFile(string fullPath)
		{
			var id = UUIDGenerator.Generate(fullPath);
			Console.WriteLine($"Start processing a big file with id: {id}");
			Directory.CreateDirectory($"C:\\{id}");

			try
			{
				ZipFile(fullPath, id);
			}
			catch (Exception e)
			{
				Console.WriteLine(e.Message);

				Directory.Delete($"C:\\{id}");
				return;
			}

			var files = Directory.GetFiles($"C:\\{id}");
			var tasks = new Task[files.Length];
			var replyTo = GetRoutingKey(files.First(f => f.EndsWith(".zip")));
			for (var i = 0; i < files.Length; i++)
			{
				var itr = i;
				var routingKey = GetRoutingKey(files[i]);
				tasks[i] = Task.Run(() => Publisher.PublishSplitFile(routingKey, id, itr+1, files.Length, File.ReadAllBytes(files[itr]), replyTo));
			}

			Task.WaitAll(tasks);

			CreateWaitingStatus(replyTo, fullPath, id);

			FileProcessingRepository.AddRow(id, $"C:\\{id}");
			File.Delete(fullPath);
		}

		private static void ZipFile(string fullPath, string id)
		{
			using (var zip = new ZipFile())
			{
				zip.AddFile(fullPath);
				zip.MaxOutputSegmentSize = 1048576;
				zip.Save($"C:\\{id}\\{id}.zip");
			}
		}

		private static void CreateWaitingStatus(string routingKey, string fullPath, string id)
		{
			var factory = new ConnectionFactory() { HostName = "localhost" };
			var connection = factory.CreateConnection();
			var channel = connection.CreateModel();
			
			channel.ExchangeDeclare(exchange: "reply_queue", type: ExchangeType.Direct);

			var queueName = channel.QueueDeclare().QueueName;

			channel.QueueBind(	queue: queueName,
								exchange: "reply_queue",
								routingKey: routingKey);

			var consumer = new EventingBasicConsumer(channel);

			consumer.Received += OnRecive;

			channel.BasicConsume(queue: queueName,
									autoAck: true,
									consumer: consumer);

			Console.WriteLine($"Subscribed to the queue '{queueName}'");

			void OnRecive(object sender, BasicDeliverEventArgs e)
			{
				if (e.BasicProperties.Headers.TryGetValue("big", out var isBigFile) && Convert.ToBoolean(isBigFile))
				{
					Directory.Delete($"C:\\{e.BasicProperties.CorrelationId}", true);
				}

				File.Delete(fullPath);

				FileProcessingRepository.DeleteRow(id);
				consumer.Received -= OnRecive;
				var body = e.Body;
				var message = Encoding.UTF8.GetString(body.ToArray());
				Console.WriteLine("Received message: {0}", message);

				channel.Close();
				connection.Close();
			}
		}
	}
}
