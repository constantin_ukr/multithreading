﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Client
{
	public static class FileProcessingRepository
	{
		private const string fileStorageName = "processingFileStorage.log";
		private const string seperator = "*";
		private static readonly Dictionary<string, string> inMemoryRepository;

		static FileProcessingRepository()
		{
			inMemoryRepository = new Dictionary<string, string>();

			if (!File.Exists(fileStorageName))
			{
				File.Create(fileStorageName);
			}
			else
			{
				foreach (var row in File.ReadAllLines(fileStorageName))
				{
					if (string.IsNullOrWhiteSpace(row))
					{
						continue;
					}

					var data = row.Split(seperator);
					inMemoryRepository.Add(data[0],data[1]);
				}
			}
		}

		public static bool Exist(string key)
		{
			return inMemoryRepository.ContainsKey(key);
		}

		public static void AddRow(string key, string path)
		{
			string text = $"{key}{seperator}{path}";

			if (inMemoryRepository.TryAdd(key, path))
			{
				File.AppendAllText(fileStorageName, $"{ Environment.NewLine}{text}");
			}
		}

		public static void DeleteRow(string key)
		{
			if (inMemoryRepository.ContainsKey(key))
			{
				var lines = File.ReadAllLines(fileStorageName).Where(row => !string.IsNullOrWhiteSpace(row) && row.Split(seperator)[0] != key).ToList();
				File.WriteAllText(fileStorageName, "");
				File.WriteAllLines(fileStorageName, lines);

				inMemoryRepository.Remove(key);
			}
		}
	}	
}
