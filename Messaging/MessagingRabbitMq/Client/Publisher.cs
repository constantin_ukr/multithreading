﻿using RabbitMQ.Client;
using System;

namespace Client
{
	class Publisher
	{

		public static void Publish(string id, string routingKey, byte[] body)
		{
			var factory = new ConnectionFactory { HostName = "localhost" };
			using (var connection = factory.CreateConnection())
			using (var channel = connection.CreateModel())
			{
				var properties = channel.CreateBasicProperties();
				properties.CorrelationId = id;
				properties.MessageId = "0";
				properties.Expiration = "60000";
				properties.ReplyTo = routingKey;

				channel.ExchangeDeclare(exchange: "ks_topic", type: ExchangeType.Topic);
				channel.BasicPublish(exchange: "ks_topic",
					routingKey: routingKey,
					basicProperties: properties,
					body: body);
			}

			Console.WriteLine($"Published simple message id: {id}");
		}

		public static void PublishSplitFile(string routingKey, string fileId, int curPartFile, int maxPartFile, byte[] body, string replyTo)
		{
			var factory = new ConnectionFactory { HostName = "localhost" };
			using (var connection = factory.CreateConnection())
			using (var channel = connection.CreateModel())
			{
				var properties = channel.CreateBasicProperties();
				properties.CorrelationId = fileId;
				properties.MessageId = $"{curPartFile}-{maxPartFile}";
				properties.Expiration = "60000";
				properties.ReplyTo = replyTo;

				channel.ExchangeDeclare(exchange: "ks_topic", type: ExchangeType.Topic);
				channel.BasicPublish(exchange: "ks_topic",
					routingKey: routingKey,
					basicProperties: properties,
					body: body);
			}

			Console.WriteLine($"Published message id: {fileId} part {curPartFile} from {maxPartFile}");
		}
	}
}
