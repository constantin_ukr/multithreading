﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace Common
{
	public class UUIDGenerator
	{
		public static string Generate(string value)
		{
			Guid result;
			using (var md5 = MD5.Create())
			{
				var hash = md5.ComputeHash(Encoding.Default.GetBytes($"{value}"));
				result = new Guid(hash);
			}

			return result.ToString();
		}
	}
}
