﻿using System;
using System.Linq;
using System.Linq.Expressions;
using MiniORM.Model.Entity;
using MiniORM.QueryProvider;

namespace MiniORM
{
	class Program
	{
		static void Main(string[] args)
		{
			var emls = new EntitySet<EmployeeEntity>().Where(e => e.FullName == "James Bond").ToList();
			var emls2 = new EntitySet<ProductEntity>().Where(e => e.Id == 1).ToList();
			var emls3 = new EntitySet<ProductEntity>().Where(e => e.Id != 1).ToList();
			var emls4 = new EntitySet<ProductEntity>().Where(e => e.Id == 1 && e.Name.StartsWith("A")).ToList();
			var emls5 = new EntitySet<ProductEntity>().Where(e => e.Name.StartsWith("A") && e.Name.EndsWith("Z")).ToList();
			var emls6 = new EntitySet<ProductEntity>().Where(e => e.Id == 1 && e.Name.Contains("Ab")).ToList();

			Console.ReadKey();
		}
	}
}
