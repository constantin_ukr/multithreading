﻿namespace MiniORM.Model.Entity
{
	public class ProductEntity :EntityBase
	{
		public int Id { get; set; }
		public string Name { get; set; }
	}
}
