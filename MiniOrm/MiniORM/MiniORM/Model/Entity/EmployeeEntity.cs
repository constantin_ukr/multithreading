﻿using System.ComponentModel.DataAnnotations.Schema;

namespace MiniORM.Model.Entity
{
	[Table("Employee")]
	public class EmployeeEntity: EntityBase
	{
		public int Id { get; set; }
		public string FullName { get; set; }
		public int Age { get; set; }
		public string JobPosition { get; set; }
	}
}
