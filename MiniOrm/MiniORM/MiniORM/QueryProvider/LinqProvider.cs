﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using MiniORM.Helpers;

namespace MiniORM.QueryProvider
{
	public class LinqProvider : IQueryProvider
	{
		public IQueryable CreateQuery(Expression expression)
		{
			throw new NotImplementedException();
		}

		public IQueryable<TElement> CreateQuery<TElement>(Expression expression)
		{
			return new Query<TElement>(expression, this);
		}

		public object Execute(Expression expression)
		{
			throw new NotImplementedException();
		}

		public TResult Execute<TResult>(Expression expression)
		{
			var itemType = TypeHelper.GetElementType(expression.Type);

			var translator = new ExpressionTranslator();
			var queryString = translator.Translate(expression);

			Console.WriteLine(queryString);

			return (TResult)(Activator.CreateInstance(typeof(List<>).MakeGenericType(itemType)) as IList);
		}
	}
}
