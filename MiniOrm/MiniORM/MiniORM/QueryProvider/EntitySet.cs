﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using MiniORM.Model.Entity;

namespace MiniORM.QueryProvider
{
	public class EntitySet<T> : IQueryable<T> where T : EntityBase
	{
		protected readonly Expression Expr;
		protected readonly IQueryProvider QueryProvider;

		public EntitySet()
		{

			Expr = Expression.Constant(this);
			QueryProvider = new LinqProvider();
		}

		#region public properties

		public Type ElementType => typeof(T);

		public Expression Expression => Expr;

		public IQueryProvider Provider => QueryProvider;

		#endregion

		#region public methods

		public IEnumerator<T> GetEnumerator()
		{
			return QueryProvider.Execute<IEnumerable<T>>(Expr).GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return QueryProvider.Execute<IEnumerable>(Expr).GetEnumerator();
		}

		#endregion
	}
}
