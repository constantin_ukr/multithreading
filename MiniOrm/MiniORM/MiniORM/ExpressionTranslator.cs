﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using MiniORM.Helpers;

namespace MiniORM
{
    public class ExpressionTranslator : ExpressionVisitor
    {
        private readonly StringBuilder _resultStringBuilder;
        private string tableAlias; 

        public ExpressionTranslator()
        {
            _resultStringBuilder = new StringBuilder();
        }

        public string Translate(Expression exp)
        {
	        Type itemType = TypeHelper.GetElementType(exp.Type);
	        var tableName = itemType.CustomAttributes.FirstOrDefault(a => a.AttributeType.Name == "TableAttribute")?.ConstructorArguments.First().Value.ToString();

	        if (string.IsNullOrWhiteSpace(tableName))
		        tableName = itemType.Name.Replace("Entity", "");

	        tableAlias = tableName.ToLower();

			_resultStringBuilder.Append($"SELECT * FROM {tableName} as {tableAlias} ");
	        
			Visit(exp);

            return _resultStringBuilder.ToString();
        }

        #region protected methods

        protected override Expression VisitMethodCall(MethodCallExpression node)
        {
            if (node.Method.DeclaringType == typeof(Queryable)
                && node.Method.Name == "Where")
            {
	            _resultStringBuilder.Append(" WHERE ");
				var predicate = node.Arguments[1];
                Visit(predicate);

                return node;
            }

			if (node.Method.DeclaringType == typeof(string))
			{
				switch (node.Method.Name)
				{
					case "StartsWith":
						Visit(node.Object);
						_resultStringBuilder.Append(" LIKE '");
						Visit(node.Arguments[0]);
						_resultStringBuilder.Append("%' ");
						return node;

					case "EndsWith":
						Visit(node.Object);
						_resultStringBuilder.Append(" LIKE '%");
						Visit(node.Arguments[0]);
						_resultStringBuilder.Append("' ");
						return node;

					case "Contains":
						Visit(node.Object);
						_resultStringBuilder.Append(" LIKE '%");
						Visit(node.Arguments[0]);
						_resultStringBuilder.Append("%' ");
						return node;

					case "Equals":
						Visit(node.Object);
						_resultStringBuilder.Append(" = '");
						Visit(node.Arguments[0]);
						_resultStringBuilder.Append("' ");
						return node;
				}
			}

			return base.VisitMethodCall(node);
		}

        protected override Expression VisitBinary(BinaryExpression node)
        {
	        if (!IsValidForEquals(node, out var isFirstMemberAccess))
		        return node;

			var operationSymbol = node.NodeType switch
            {
                ExpressionType.Equal => " = ",
                ExpressionType.AndAlso => " AND ",
				ExpressionType.And => " AND ",
				ExpressionType.NotEqual => " <> ",
				ExpressionType.Or => " OR ", 
				_ => throw new NotSupportedException($"Operation '{node.NodeType}' is not supported")
            };

			if (operationSymbol == "=" || operationSymbol == "!=")
			{
				Visit(node.Left);
				_resultStringBuilder.Append($"{operationSymbol}");
				Visit(node.Right);
			}
			else
				DoCorrectVisit(isFirstMemberAccess, operationSymbol);

			return node;

            void DoCorrectVisit(bool isFirstMemberAccess, string operationSymbol)
            {
	            if (isFirstMemberAccess)
	            {
		            Visit(node.Left);
		            _resultStringBuilder.Append($" {operationSymbol} ");
		            ProcessConstant();
	            }
	            else
	            {
		            ProcessConstant();
		            _resultStringBuilder.Append($" {operationSymbol} ");
		            Visit(node.Left);
	            }

	            void ProcessConstant()
	            {
		            if (node.Right.Type.Name == "String")
		            {
			            _resultStringBuilder.Append("'");
			            Visit(node.Right);
			            _resultStringBuilder.Append("'");
		            }
		            else
		            {
			            Visit(node.Right);
		            }
				}
            }
        }

        protected override Expression VisitMember(MemberExpression node)
        {
	        _resultStringBuilder.Append( $"[{tableAlias}].[{node.Member.Name}]");

            return base.VisitMember(node);
        }

        protected override Expression VisitConstant(ConstantExpression node)
        {
			_resultStringBuilder.Append(node.Value);
			
			return node;
        }

        #endregion


        private bool IsValidForEquals(BinaryExpression node, out bool isFirstMemberAccess)
        {
	        isFirstMemberAccess = true;
	        if (node.NodeType == ExpressionType.AndAlso || node.NodeType == ExpressionType.And)
		        return true;

	        ExpType e = node.Left.NodeType == ExpressionType.MemberAccess
		        ? ExpType.MemberAccess
		        : node.Left.NodeType == ExpressionType.Constant
			        ? ExpType.Constant : ExpType.None;

	        isFirstMemberAccess = e.HasFlag(ExpType.MemberAccess);

            e |= node.Right.NodeType == ExpressionType.MemberAccess
	            ? ExpType.MemberAccess
	            : node.Right.NodeType == ExpressionType.Constant
		            ? ExpType.Constant : ExpType.None;

            var v = e.HasFlag(ExpType.Both);
            return v;
        }

        [Flags]
        private enum ExpType
        {
            None,
            MemberAccess,
            Constant,
            Both = MemberAccess | Constant
        }
    }
}
